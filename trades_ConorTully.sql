--Find all trades by a given trader on a given stock - for example the trader with ID=1 and the ticker 'MRK'. 
--This involves joining from trader to position and then to trades twice, through the opening- and closing-trade FKs.
SELECT trader.ID, trade1.stock
FROM dbo.trader
JOIN dbo.position ON position.trader_id = trader.ID
JOIN dbo.trade trade1 ON trade1.id = position.opening_trade_ID
JOIN dbo.trade trade2 ON trade2.id = position.closing_trade_ID;

--Find the total profit or loss for a given trader over the day, 
--as the sum of the product of trade size and price for all sales, 
--minus the sum of the product of size and price for all buys.
SELECT (SUM(trade.size * trade.price) - 
(SELECT SUM(trade.size * trade.price)
FROM dbo.trade
JOIN dbo.position ON position.opening_trade_ID = trade.ID
JOIN dbo.trader ON trader.ID = position.trader_id
WHERE trade.buy = 0))
FROM dbo.trade
JOIN dbo.position ON position.opening_trade_ID = trade.ID
JOIN dbo.trader ON trader.ID = position.trader_id
WHERE trade.buy = 1;

--Develop a view that shows profit or loss for all traders.
CREATE VIEW TotalProfitLoss AS
SELECT (SUM(trade.size * trade.price) - 
(SELECT SUM(trade.size * trade.price)
FROM dbo.trade
JOIN dbo.position ON position.opening_trade_ID = trade.ID
JOIN dbo.trader ON trader.ID = position.trader_id
WHERE trade.buy = 0)) AS TotalProfitLoss
FROM dbo.trade
JOIN dbo.position ON position.opening_trade_ID = trade.ID
JOIN dbo.trader ON trader.ID = position.trader_id
WHERE trade.buy = 1;
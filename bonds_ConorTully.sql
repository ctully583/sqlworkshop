--Show all information about the bond with the CUSIP '28717RH95'.
SELECT * FROM dbo.bond WHERE CUSIP = '28717RH95';

--Show all information about all bonds, in order from shortest (earliest maturity) to longest (latest maturity).
SELECT * FROM dbo.bond
ORDER BY maturity ASC;

--Calculate the value of this bond portfolio, as the sum of the product of each bond's quantity and price.
SELECT SUM(quantity * price) FROM dbo.bond;

--Show the annual return for each bond, as the product of the quantity and the coupon. 
--Note that the coupon rates are quoted as whole percentage points, so to use them 
--here you will divide their values by 100.
SELECT CUSIP, (quantity * (coupon / 100)) AS AnnualReturn
FROM dbo.bond
ORDER BY CUSIP;

--Show bonds only of a certain quality and above, for example those bonds with ratings at least AA2. 
--(Don't resort to regular-expression or other string-matching tricks; use the bond-rating ordinal.)
SELECT * FROM dbo.bond LEFT OUTER JOIN dbo.rating ON rating.rating = bond.rating WHERE ordinal <= 3;

--Show the average price and coupon rate for all bonds of each bond rating.
SELECT rating, AVG(price) AS AveragePrice, AVG(coupon) AS AverageCoupon FROM dbo.bond GROUP BY rating;

--Calculate the yield for each bond, as the ratio of coupon to price. 
--Then, identify bonds that we might consider to be overpriced, 
--as those whose yield is less than the expected yield given the rating of the bond.
SELECT CUSIP, (coupon / price) AS yield 
FROM dbo.bond JOIN dbo.rating ON rating.rating = bond.rating 
WHERE expected_yield > (coupon / price);